from django.urls import path
from admin_app.views import UserView, UserList


urlpatterns = [
    path(r'user/create/', UserView.as_view()),
    path(r'user/', UserList.as_view())
]
