def get_string_without_int(string):
    return ''.join([x for x in string if x.isalpha()])


def get_int_without_string(string):
    try:
        number = int(''.join([x for x in string if x.isdigit()]))
    except ValueError:
        number = 0
    return number
