from transliterate import translit
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

from admin_app.utils import get_int_without_string, get_string_without_int

settings.AUTH_USER_MODEL = 'admin_app.User'


class User(AbstractUser):
    patronymic = models.CharField(max_length=30)

    def save(self, *args, **kwargs):
        self.unique_or_new_username()
        super().save(*args, **kwargs)

    def transliterate_username(self):
        username = self.last_name + self.first_name[0] + self.patronymic[0]
        return translit(username, 'ru', reversed=True)

    def username_is_unique(self):
        try:
            User.objects.get(username=self.username)
        except User.DoesNotExist:
            return True
        else:
            return False

    def unique_or_new_username(self):
        """
        check the uniqueness of the username and return new username
        if username is not unique
        """
        self.username = self.transliterate_username()
        if self.username_is_unique():
            return self.username
        users = User.objects.filter(
            username__startswith=get_string_without_int(self.username)
            ).order_by('-pk')
        number = get_int_without_string(users[0].username) + 1
        self.username += str(number)
