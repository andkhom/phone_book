from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.permissions import IsAdminUser

from admin_app.serializers import CreateUserSerializer
from admin_app.models import User


class UserView(CreateAPIView):
    serializer_class = CreateUserSerializer
    permission_classes = (IsAdminUser,)


class UserList(ListAPIView):
    serializer_class = CreateUserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAdminUser,)
