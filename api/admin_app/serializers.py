from rest_framework import serializers

from admin_app.models import User


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('password', 'patronymic', 'first_name', 'last_name')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User()
        for key in validated_data:
            if key == 'password':
                user.set_password(validated_data['password'])
                continue
            setattr(user, key, validated_data[key])
        user.save()
        return user
