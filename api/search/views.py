from rest_framework.generics import ListAPIView
from main.models import Organization, Worker
from main.serializers import OrganizationSerializer, WorkerSerializer


# bug with sqlite
# https://docs.djangoproject.com/en/2.1/ref/databases/#sqlite-string-matching
class WorkerSearchView(ListAPIView):
    serializer_class = WorkerSerializer

    def get_queryset(self):
        return Worker.objects.filter(
            surname__contains=self.kwargs['search']
        )


class OrganizationSearchView(ListAPIView):
    serializer_class = OrganizationSerializer

    def get_queryset(self):
        return Organization.objects.filter(
            full_name__icontains=self.kwargs['search']
        )
