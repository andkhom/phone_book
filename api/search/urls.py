from django.urls import path
from search.views import OrganizationSearchView, WorkerSearchView


urlpatterns = [
    path('organization/name=<str:search>', OrganizationSearchView.as_view()),
    path('worker/name=<str:search>', WorkerSearchView.as_view())
]
