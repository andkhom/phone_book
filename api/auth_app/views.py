from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView
)

from .serializers import (
    TokenWithExpireTimePairSerializer,
    TokenWithExpireTimeRefreshSerializer
)


class TokenWithExpireTimeObtainPairView(TokenObtainPairView):
    serializer_class = TokenWithExpireTimePairSerializer


class TokenWithExpireTimeRefreshView(TokenRefreshView):
    serializer_class = TokenWithExpireTimeRefreshSerializer
