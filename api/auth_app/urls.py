from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path(r'token/', TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path(r'token/refresh/', TokenRefreshView.as_view(),
         name='token_refresh'),
]
