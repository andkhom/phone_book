from django.utils.six import text_type
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from rest_framework_simplejwt.settings import api_settings


class TokenWithExpireTimePairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        token = self.get_token(self.user)

        data['access_expire'] = token.access_token.payload['exp']
        data['refresh_expire'] = token.payload['exp']

        return data


class TokenWithExpireTimeRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        refresh = RefreshToken(attrs['refresh'])

        data = {'access': text_type(refresh.access_token)}
        data['access_expire'] = refresh.access_token.payload['exp']

        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    refresh.blacklist()
                except AttributeError:
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = text_type(refresh)
            data['refresh_expire'] = refresh.payload['exp']

        return data
