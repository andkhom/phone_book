from django.db import models


class Organization(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=128)
    full_name = models.CharField(max_length=256, unique=True)
    phone = models.CharField(max_length=32, blank=True)
    phone_2 = models.CharField(max_length=32, blank=True)
    email = models.CharField(max_length=128, blank=True)


class Worker(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=128, blank=True)
    surname = models.CharField(max_length=128)
    patronymic = models.CharField(max_length=128, blank=True)
    is_head = models.BooleanField(default=False)
    organization = models.ForeignKey(
        Organization,
        on_delete=models.SET_NULL,
        null=True)

    def __str__(self):
        return '{} {} {}'.format(self.surname, self.name, self.patronymic)
