from rest_framework import routers
from main.views import OrganizationViewSet, WorkerViewSet


router = routers.SimpleRouter()
router.register(r'organization', OrganizationViewSet)
router.register(r'worker', WorkerViewSet)

urlpatterns = router.urls
