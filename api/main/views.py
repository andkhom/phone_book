from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from main.models import Organization, Worker
from main.serializers import OrganizationSerializer, WorkerSerializer


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated,)


class WorkerViewSet(viewsets.ModelViewSet):
    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer
    permission_classes = (IsAuthenticated,)
